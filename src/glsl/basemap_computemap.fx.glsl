/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**  Copyright 2015 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/
// include functions for projections
#include "projections.glsl"


/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

// Shader buffer storage object of the computed basemap vertices.
layout (std430, binding=0) buffer BasemapBuffer
{
    ProjectionPoint basemapGridPoints[];
};

layout (std430, binding=1) buffer IndexBuffer
{
    uint basemapIndices[];
};

// projection variables.
uniform float centralLon;
uniform float centralLat;
uniform int   projectionMethod;
uniform int   projectionDirection; //1 for forward, -1 for inverse
uniform bool  rotated;
uniform bool  checkConnection;

// resolution of the map in degrees
uniform float mapResolutionX;
uniform float mapResolutionY;
// map dimensions
uniform int numLon;
uniform int numLat;

// bounding box corners.
uniform vec4  cornersBox;


/*****************************************************************************
 ***                           COMPUTE SHADER
 *****************************************************************************/

// Shader that computes projected basemap
shader CSbasemap()
{
    if(gl_GlobalInvocationID.x >= numLon || gl_GlobalInvocationID.y >= numLat)
    {
        return;
    }
    uint index = gl_GlobalInvocationID.y * numLon + gl_GlobalInvocationID.x;

    float lon = cornersBox.x + gl_GlobalInvocationID.x * mapResolutionX;
    float lat = cornersBox.y + gl_GlobalInvocationID.y * mapResolutionY;

    vec2 geoPosition = vec2(0., 0.);
    vec2 mapProjection = vec2(0., 0.);

    if (projectionDirection == PROJ_FWD)
    {
        geoPosition = vec2(lon, lat);
        mapProjection = projectGeoToMap(geoPosition, vec2(centralLon, centralLat), projectionMethod);
    }
    else if (projectionDirection == PROJ_INV)
    {
        mapProjection = vec2(lon, lat);
        geoPosition = projectMapToGeo(mapProjection, vec2(centralLon, centralLat), projectionMethod);
    }


    basemapGridPoints[index].mapProjection = mapProjection;
    basemapGridPoints[index].geoPosition = geoPosition;
}

// Shader that computes index a buffer for a projected basemap.
shader CSindex()
{
    if(gl_GlobalInvocationID.x >= numLon - 1 || gl_GlobalInvocationID.y >= numLat - 1)
    {
        return;
    }

    uint indexMap = gl_GlobalInvocationID.y * numLon + gl_GlobalInvocationID.x;
    uint indexIndex = (gl_GlobalInvocationID.y * (numLon - 1) + gl_GlobalInvocationID.x) * 6;


    if(projectionMethod == EQUIRECTANGULAR && !rotated)
    {
        basemapIndices[0] = 0;
        basemapIndices[1] = 1;
        basemapIndices[2] = 2;

        basemapIndices[3] = 1;
        basemapIndices[4] = 3;
        basemapIndices[5] = 2;
    }
    else if(checkConnection)
    {
        if(validConnection(basemapGridPoints[indexMap],
                           basemapGridPoints[indexMap + 1],
                           vec2(centralLon, centralLat),
                           projectionMethod)
        && validConnection(basemapGridPoints[indexMap],
                           basemapGridPoints[indexMap + numLon],
                           vec2(centralLon, centralLat),
                           projectionMethod))
        {
            basemapIndices[indexIndex] = indexMap;
            basemapIndices[indexIndex + 1] = indexMap + 1;
            basemapIndices[indexIndex + 2] = indexMap + numLon;
        }
        else
        {
            //remove the triangle by making it invisible
            basemapIndices[indexIndex] = 0;
            basemapIndices[indexIndex + 1] = 0;
            basemapIndices[indexIndex + 2] = 0;
        }

        if(validConnection(basemapGridPoints[indexMap + numLon + 1],
                           basemapGridPoints[indexMap + numLon],
                           vec2(centralLon, centralLat),
                           projectionMethod)
        && validConnection(basemapGridPoints[indexMap + 1],
                           basemapGridPoints[indexMap + numLon + 1],
                           vec2(centralLon, centralLat),
                           projectionMethod))
        {
            basemapIndices[indexIndex + 3] = indexMap + 1;
            basemapIndices[indexIndex + 4] = indexMap + numLon + 1;
            basemapIndices[indexIndex + 5] = indexMap + numLon;
        }
        else
        {
            //remove the triangle by making it invisible
            basemapIndices[indexIndex + 3] = 0;
            basemapIndices[indexIndex + 4] = 0;
            basemapIndices[indexIndex + 5] = 0;
        }
    }
    else
    {
        basemapIndices[indexIndex] = indexMap;
        basemapIndices[indexIndex + 1] = indexMap + 1;
        basemapIndices[indexIndex + 2] = indexMap + numLon;

        basemapIndices[indexIndex + 3] = indexMap + 1;
        basemapIndices[indexIndex + 4] = indexMap + numLon + 1;
        basemapIndices[indexIndex + 5] = indexMap + numLon;
    }
}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program ComputeBasemap
{
    cs(430)=CSbasemap() : in(local_size_x = 32, local_size_y = 32);
};

program ComputeBasemapIndex
{
    cs(430)=CSindex() : in(local_size_x = 32, local_size_y = 32);
};